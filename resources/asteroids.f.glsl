#version 330 core

in vec4 inColor;

uniform float t;

layout (location = 0) out vec4 outColor;

void main()
{
    outColor = inColor;
}
