#version 330 core

in vec4 color;
in vec4 position;

uniform float ar;

out vec4 C;

void main() {
    gl_Position = vec4(position.x, position.y * ar, position.zw);
    C = color;
}
