#version 330 core

in vec2 vertex;
in vec4 geometry;
in vec4 color;

uniform float ar;

out vec4 inColor;

void main()
{
    float th = geometry.z;
    vec3 pos = mat3(
        cos(th), sin(th), 0.0,
        -sin(th), cos(th), 0.0,
        geometry.x, geometry.y, 0.0
    ) * vec3(vertex.xy * geometry.w, 1.0);
    gl_Position = vec4(pos.x, pos.y * ar, pos.z, 1.0);
    inColor = color;
}
