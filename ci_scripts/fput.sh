#!/bin/bash

export TS=$(date +%Y-%m-%d.%H-%M)
for plat in win32 win64 linux; do
    curl \
      -F "key=$FPUT_KEY" \
      -F "path=asteroids/asteroids-$TS-${CI_COMMIT_SHA:0:8}-$plat.zip" \
      -F "file=@asteroids-$plat.zip" \
      $FPUT_URL
done
