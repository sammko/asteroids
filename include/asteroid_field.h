#pragma once
#include <vector>
#include <random>

#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glad/glad.h>

#include "types.h"
#include "shaders.h"

namespace Asteroids {

    const size_t SHAPES_COUNT = 16;
    const size_t MIN_ASTEROID_VERTICES = 5;

    const size_t NPLAYERS = 3;

    struct vbo_entry_t {
        glm::vec4 geometry, color;
    };

    enum polygon_type_t {
        OBJECT_ASTEROID,
        OBJECT_SHIP
    };

    struct Polygon {
        virtual polygon_type_t type() const = 0;
        int shape_id = 0;
        glm::vec2 position = glm::vec2(0, 0);
        glm::vec2 velocity = glm::vec2(0, 0);
        glm::vec3 color = glm::vec3(1.0, 1.0, 1.0);
        float size = 1.0f;
        float theta = 0.0f;
        float angular_velocity = 0.0f;
        virtual void update(float dt) = 0;
        virtual bool is_dead() const = 0;
        vbo_entry_t get_vbo_entry() {
            return (vbo_entry_t) {
                .geometry = glm::vec4(position, theta, size),
                .color = glm::vec4(color, 1.0)
            };
        }
    };

    struct Asteroid : Polygon {
        polygon_type_t type() const {
            return OBJECT_ASTEROID;
        }
        bool dead = false;
        static Asteroid* make_random();
        static Asteroid* make_random_at(int type, glm::vec2 position);
        void update(float dt);
        bool is_dead() const {
            return this->dead;
        }
    };

    struct Ship : Polygon {
        polygon_type_t type() const {
            return OBJECT_SHIP;
        }
        const float acceleration = .5;
        const float angular_acceleration = 20.0;
        const float angular_drag = 0.94;
        input_mode_t input_source = INPUT_MODE_KEYBOARD;
        void update(float dt);
        bool is_dead() const {
            return false;
        }
    };

    class AsteroidField {
    public:
        void init();
        void destroy();
        void draw();
        void update(float dt);
        void reset_ships();
        ~AsteroidField();
    private:
        void upload_instance_data();
        void prepare_shapes();
        void create_ships();
        GLuint vao;
        GLuint vbo_vertices, vbo_instances, ebo_shapes;
        ShaderProgram *shader_program;
        struct {
            GLint vertex, geometry, color;
        } shader_attr_locations;
        struct {
            GLint ar;
        } shader_uniform_locations;
        std::vector<vbo_entry_t> vbo_instances_data;
        std::vector<Polygon*> objects;
        std::vector<GLushort> ebo_shapes_data;
        std::vector<ofs_t> shape_offsets;
        size_t shapes_counts[SHAPES_COUNT];
        Ship *ships[NPLAYERS];
    };
}
