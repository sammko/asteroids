#pragma once

namespace Asteroids {
    struct ofs_t {
        size_t offset, size;
    };

    enum input_mode_t {
        INPUT_MODE_KEYBOARD,
        INPUT_MODE_KEYBOARD2,
        INPUT_MODE_GP1,
        INPUT_MODE_GP2,
        INPUT_MODE_GP3,
        INPUT_MODE_GP4,
        INPUT_MODE_GP5,
        INPUT_MODE_GP6,
        INPUT_MODE_GP7,
        INPUT_MODE_GP8
    };

    struct input_state_t {
        bool shoot;
        float left, right, accelerate;
    };
}