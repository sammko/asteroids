#pragma once
#include <cmath>

namespace Asteroids {
    constexpr double pi = 3.141592653589793;
}

namespace Asteroids {
    namespace Random {
        extern void seed(unsigned);
        extern float get_f(float min, float max);
        extern int get_i(int min, int max);
        extern float get_nf(float mean, float sdev);
    }
}