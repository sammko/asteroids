#pragma once

#include <string>

#include <glad/glad.h>


namespace Asteroids {
    class ShaderProgram {
        GLuint vsh, fsh, program;
    public:
        ShaderProgram(const std::string &f_vsh, const std::string &f_fsh);
        ~ShaderProgram();
        GLuint get_id() const { return program; }
    };
}