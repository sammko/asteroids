#pragma once

#include <vector>

#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>

#include "shaders.h"

namespace Asteroids {

    struct particle_vbo_entry_t {
        glm::vec4 color, position;
    };

    struct Particle {
        glm::vec2 position;
        glm::vec3 color;
        glm::vec2 velocity;
        float life;
        bool dead;
        void update(float dt);
        particle_vbo_entry_t get_vbo_entry() {
            return (particle_vbo_entry_t) {
                .color = glm::vec4(color, 1.0),
                .position = glm::vec4(position, 0.0, 1.0)
            };
        }
    };

    class ParticleRenderer {
        float ar;
        GLuint vao, vbo;
        ShaderProgram *shader;
        struct {
            GLuint color, position;
        } shader_attr_locations;
        struct {
            GLuint ar;
        } shader_uniform_locations;
        std::vector<Particle> particles;
        std::vector<particle_vbo_entry_t> vbo_particles_data;
    public:
        void init();
        void destroy();
        void draw();
        void update(float dt);
        void spawn(Particle p) {
            this->particles.push_back(p);
        }
        void spawn_burst(glm::vec2 position, glm::vec3 color,
                         glm::vec3 color_deviation,
                         float theta, float dispersion, float life,
                         glm::vec2 base_velocity, size_t count);
    };
}