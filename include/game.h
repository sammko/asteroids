#pragma once

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include "asteroid_field.h"
#include "particles.h"

namespace Asteroids {
    const int TARGET_WIDTH = 640;
    const int TARGET_HEIGHT = 360;

    struct viewport_t {
        GLint x, y;
        GLsizei width, height;
    };

    class Game {
        GLFWwindow *window;
        viewport_t window_viewport, screen_viewport, render_viewport;

        float ar;

        AsteroidField asteroid_field;
        ParticleRenderer particle_renderer;
        input_state_t keyboard_inp = {0};
        input_state_t keyboard_inp2 = {0};

        struct {
            struct {
                GLuint fbo;
                GLuint rbo_color;
            } target_fbo;
        } gl_objects;

        static void framebuffer_size_cb(GLFWwindow*, int, int);
        static void error_cb(int, const char*);
        static void key_cb(GLFWwindow*, int, int, int, int);

        void prepare_target_fbo();
        void prepare_drawables();
        void destroy_drawables();
        void set_ar(int width, int height);
    public:
        Game();
        ~Game();
        void run();
        float get_ar() const {
            return this->ar;
        }
        input_state_t get_input(input_mode_t im);
        ParticleRenderer *get_particle_renderer() {
            return &this->particle_renderer;
        }
    };

    extern Game *g_game;
}