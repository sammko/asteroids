#include <stdexcept>
#include <algorithm>
#include <iostream>
#include <string>
#include <cstdlib>
#include <ctime>

#include "game.h"
#include "file_util.h"
#include "resources.h"
#include "shaders.h"
#include "types.h"
#include "util.h"

using namespace Asteroids;

namespace Asteroids {
    Game *g_game;
}

void load_controller_mappings() {
    char *s = file_contents(RESOURCE_DIR "gamecontrollerdb.txt", nullptr);
    if (!s) return;
    glfwUpdateGamepadMappings(s);
    printf("Loaded gamecontrollerdb.txt\n");
    free(s);
}

void set_viewport(const viewport_t &v) {
    glViewport(v.x, v.y, v.width, v.height);
}

void Game::framebuffer_size_cb(GLFWwindow *window, int width, int height) {
    Game *game = (Game*)glfwGetWindowUserPointer(window);
    float scale = std::min(width / (float)TARGET_WIDTH,
                           height / (float)TARGET_HEIGHT);
    int vw = (int)(scale * TARGET_WIDTH);
    int vh = (int)(scale * TARGET_HEIGHT);
    game->screen_viewport = {
        .x = (width - vw) / 2,
        .y = (height - vh) / 2,
        .width = vw,
        .height = vh
    };
    game->window_viewport = {
        .x = 0,
        .y = 0,
        .width = width,
        .height = height
    };
}

void Game::error_cb(int code, const char *message) {
    fprintf(stderr, "[GLFW Error] %d: %s\n", code, message);
}

void Game::key_cb(GLFWwindow *window, int key,
                  int scancode, int action, int mods)
{
    Game *game = (Game*)glfwGetWindowUserPointer(window);
    if (action != GLFW_PRESS && action != GLFW_RELEASE) return;
    bool state = action == GLFW_PRESS;
    switch (key) {
        case GLFW_KEY_Z: game->keyboard_inp.shoot = state; break;
        case GLFW_KEY_UP: game->keyboard_inp.accelerate = state * 1.f; break;
        case GLFW_KEY_LEFT: game->keyboard_inp.left = state * 1.f; break;
        case GLFW_KEY_RIGHT: game->keyboard_inp.right = state * 1.f; break;
        case GLFW_KEY_ESCAPE: glfwSetWindowShouldClose(window, true); break;
        case GLFW_KEY_R: game->asteroid_field.reset_ships(); break;
        case GLFW_KEY_Q: game->keyboard_inp2.shoot = state; break;
        case GLFW_KEY_W: game->keyboard_inp2.accelerate = state * 1.f; break;
        case GLFW_KEY_A: game->keyboard_inp2.left = state * 1.f; break;
        case GLFW_KEY_D: game->keyboard_inp2.right = state * 1.f; break;
    }
}

input_state_t Game::get_input(input_mode_t im) {
    if (im == INPUT_MODE_KEYBOARD) {
        return this->keyboard_inp;
    } else if (im == INPUT_MODE_KEYBOARD2) {
        return this->keyboard_inp2;
    }else {
        int jsid = im - 1;
        GLFWgamepadstate gpstate;
        input_state_t s = {0};
        if (!glfwJoystickPresent(jsid) || !glfwJoystickIsGamepad(jsid))
            return s;
        glfwGetGamepadState(jsid, &gpstate);
        s.accelerate = gpstate.buttons[GLFW_GAMEPAD_BUTTON_RIGHT_BUMPER]
            ? 1.0f : (gpstate.axes[GLFW_GAMEPAD_AXIS_RIGHT_TRIGGER] + 1.f) / 2.f;
        float a = gpstate.axes[GLFW_GAMEPAD_AXIS_LEFT_X];
        a = (abs(a) < 0.1) ? 0.0 : a;
        s.left = gpstate.buttons[GLFW_GAMEPAD_BUTTON_DPAD_LEFT]
            ? 1.0f : (a < 0 ? -a : 0.f);
        s.right = gpstate.buttons[GLFW_GAMEPAD_BUTTON_DPAD_RIGHT]
            ? 1.0f : (a > 0 ? a : 0.f);
        s.shoot = gpstate.buttons[GLFW_GAMEPAD_BUTTON_CROSS];
        if (gpstate.buttons[GLFW_GAMEPAD_BUTTON_BACK])
            g_game->asteroid_field.reset_ships();
        return s;
    }
}

Game::Game() {
    g_game = this;
    if (!glfwInit())
        throw(std::runtime_error("Failed to initialize GLFW."));

    glfwSetErrorCallback(error_cb);

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE);

    GLFWmonitor *monitor = glfwGetPrimaryMonitor();
    const GLFWvidmode *vidmode = glfwGetVideoMode(monitor);

    this->window = glfwCreateWindow(vidmode->width, vidmode->height,
                                    "Asteroids", monitor, NULL);
    if (!this->window)
        throw(std::runtime_error("Failed to create glfw window."));

    /* For use in static callbacks */
    glfwSetWindowUserPointer(this->window, this);
    glfwSetInputMode(this->window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
    glfwMakeContextCurrent(this->window);

    load_controller_mappings();

    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
        throw(std::runtime_error("Failed to intialize OpenGL."));

    printf("Running with OpenGL: %s\n", glGetString(GL_VERSION));

    glfwSetFramebufferSizeCallback(window, framebuffer_size_cb);
    glfwSetKeyCallback(window, key_cb);

    { /* Not sure if this is necessary, research required */
        int width, height;
        glfwGetFramebufferSize(this->window, &width, &height);
        framebuffer_size_cb(this->window, width, height);
    }

    // disable vsync
    // glfwSwapInterval(0);

    Random::seed(time(NULL));

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    prepare_target_fbo();

    prepare_drawables();
}

Game::~Game() {
    destroy_drawables();
    glfwDestroyWindow(this->window);
    glfwTerminate();
}

void Game::prepare_target_fbo() {
    glGenRenderbuffers(1, &gl_objects.target_fbo.rbo_color);
    glBindRenderbuffer(GL_RENDERBUFFER, gl_objects.target_fbo.rbo_color);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_RGBA8, TARGET_WIDTH,
                          TARGET_HEIGHT);
    glBindRenderbuffer(GL_RENDERBUFFER, 0);

    glGenFramebuffers(1, &gl_objects.target_fbo.fbo);
    glBindFramebuffer(GL_FRAMEBUFFER, gl_objects.target_fbo.fbo);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                              GL_RENDERBUFFER, gl_objects.target_fbo.rbo_color);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    this->render_viewport = {
        .x = 0,
        .y = 0,
        .width = TARGET_WIDTH,
        .height = TARGET_HEIGHT,
    };
    this->ar = (float)TARGET_WIDTH / TARGET_HEIGHT;
}

void Game::prepare_drawables() {
    asteroid_field.init();
    particle_renderer.init();

}

void Game::destroy_drawables() {
    asteroid_field.destroy();
    particle_renderer.destroy();

}

void Game::run() {
    float t = 0.0;
    float dt = 1.0f/100;
    float current_time = glfwGetTime();
    float acc = dt; // Force update before 1st draw

    int counter = 0;
    int frames = 0;

    while (!glfwWindowShouldClose(window)) {
        float new_time = glfwGetTime();
        float frame_time = new_time - current_time;
        current_time = new_time;
        acc += frame_time;
        frames++;

        while (acc >= dt) {
            glfwPollEvents();
            if (!(++counter % (int)(1/dt))) {
                std::cout << "FPS: " << frames << std::endl;
                counter = frames = 0;
            }
            // Update
            asteroid_field.update(dt);
            particle_renderer.update(dt);

            t += dt;
            acc -= dt;
        }

        glBindFramebuffer(GL_FRAMEBUFFER, gl_objects.target_fbo.fbo);

        glClear(GL_COLOR_BUFFER_BIT);
        set_viewport(render_viewport);

        //draw stuff here

        particle_renderer.draw();
        asteroid_field.draw();

        glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
        glClear(GL_COLOR_BUFFER_BIT);
        set_viewport(window_viewport);

        glBindFramebuffer(GL_READ_FRAMEBUFFER, gl_objects.target_fbo.fbo);
        glBlitFramebuffer(0, 0, TARGET_WIDTH, TARGET_HEIGHT,
                          screen_viewport.x, screen_viewport.y,
                          screen_viewport.x + screen_viewport.width,
                          screen_viewport.y + screen_viewport.height,
                          GL_COLOR_BUFFER_BIT, GL_NEAREST);

        glfwSwapBuffers(window);

    }
}
