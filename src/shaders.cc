#include <stdexcept>
#include <string>

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cerrno>
#include <glad/glad.h>

#include "file_util.h"
#include "shaders.h"

using namespace Asteroids;

void show_info_log(
    GLuint object,
    PFNGLGETSHADERIVPROC glGet__iv,
    PFNGLGETSHADERINFOLOGPROC glGet__InfoLog
) {
    GLint log_length;
    char *log;

    glGet__iv(object, GL_INFO_LOG_LENGTH, &log_length);
    log = (char*)malloc(log_length);
    glGet__InfoLog(object, log_length, NULL, log);
    fprintf(stderr, "%s", log);
    free(log);
}

GLuint make_shader(GLenum type, const char *filename) {
    GLsizei length;
    GLchar *source = file_contents(filename, &length);
    if (!source) {
        fprintf(stderr, "Failed to read %s: %s", filename, strerror(errno));
        return 0;
    }
    GLuint shader;
    GLint shader_ok;

    shader = glCreateShader(type);
    glShaderSource(shader, 1, (const GLchar**)&source, &length);
    glCompileShader(shader);

    glGetShaderiv(shader, GL_COMPILE_STATUS, &shader_ok);
    if (!shader_ok) {
        fprintf(stderr, "Failed to compile %s:\n", filename);
        show_info_log(shader, glGetShaderiv, glGetShaderInfoLog);
        glDeleteShader(shader);
        return 0;
    }
    free(source);
    return shader;
}

GLuint make_program(GLuint vertex_shader, GLuint fragment_shader) {
    GLint program_ok;
    GLuint program = glCreateProgram();

    glAttachShader(program, vertex_shader);
    glAttachShader(program, fragment_shader);

    glBindFragDataLocation(program, 0, "outColor");

    glLinkProgram(program);

    glGetProgramiv(program, GL_LINK_STATUS, &program_ok);
    if (!program_ok) {
        fprintf(stderr, "Failed to link shader program:\n");
        show_info_log(program, glGetProgramiv, glGetProgramInfoLog);
        glDeleteProgram(program);
        return 0;
    }
    return program;
}

ShaderProgram::ShaderProgram(const std::string &f_vsh,
                             const std::string &f_fsh) {
    vsh = make_shader(GL_VERTEX_SHADER, f_vsh.c_str());
    if (!vsh) throw(std::runtime_error(""));
    fsh = make_shader(GL_FRAGMENT_SHADER, f_fsh.c_str());
    if (!fsh) throw(std::runtime_error(""));

    program = make_program(vsh, fsh);
    if (!program) throw(std::runtime_error(""));
}

ShaderProgram::~ShaderProgram() {
    glDeleteProgram(program);
    glDeleteShader(vsh);
    glDeleteShader(fsh);
}
