#include <stdio.h>
#include <stdlib.h>
#include "file_util.h"

char *file_contents(const char *filename, int *p_length) {
    // TODO: rewrite to C++
    size_t length;
    FILE *f = fopen(filename, "r");
    if (!f) return NULL;
    fseek(f, 0, SEEK_END);
    length = ftell(f);
    fseek(f, 0, SEEK_SET);

    char *buffer = (char*)malloc(length+1);
    length = fread(buffer, 1, length, f);
    fclose(f);
    buffer[length] = '\0';

    if (p_length) *p_length = length;
    return buffer;
}