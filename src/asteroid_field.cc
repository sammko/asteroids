#include <cstdio>
#include <cmath>

#include <glm/geometric.hpp>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include "asteroid_field.h"
#include "shaders.h"
#include "resources.h"
#include "util.h"
#include "game.h"

using namespace Asteroids;

namespace Asteroids {
    namespace GLData {
        static GLfloat vbo_asteroid_vertices_data[] {
            // disgusting
            0.7000000000000000f, 0.0000000000000000f,
            0.6062177826491071f, 0.3499999999999999f,
            0.3500000000000000f, 0.6062177826491070f,
            0.0000000000000000f, 0.7000000000000000f,
            -0.3499999999999998f, 0.6062177826491071f,
            -0.6062177826491071f, 0.3499999999999999f,
            -0.7000000000000000f, 0.0000000000000001f,
            -0.6062177826491070f, -0.3500000000000000f,
            -0.3500000000000003f, -0.6062177826491069f,
            -0.0000000000000001f, -0.7000000000000000f,
            0.3500000000000000f, -0.6062177826491070f,
            0.6062177826491069f, -0.3500000000000003f,

            1.0000000000000000f, 0.0000000000000000f,
            0.8660254037844387f, 0.4999999999999999f,
            0.5000000000000001f, 0.8660254037844386f,
            0.0000000000000001f, 1.0000000000000000f,
            -0.4999999999999998f, 0.8660254037844387f,
            -0.8660254037844387f, 0.4999999999999999f,
            -1.0000000000000000f, 0.0000000000000001f,
            -0.8660254037844386f, -0.5000000000000001f,
            -0.5000000000000004f, -0.8660254037844384f,
            -0.0000000000000002f, -1.0000000000000000f,
            0.5000000000000001f, -0.8660254037844386f,
            0.8660254037844384f, -0.5000000000000004f,
            -0.3, 0, // ship center
        };
    }
}

void AsteroidField::draw() {
    glUseProgram(this->shader_program->get_id());
    glBindVertexArray(this->vao);
    for (size_t i = 0, j = 0; i < SHAPES_COUNT; ++i) {
        if (!shapes_counts[i]) continue;
        glDrawElementsInstancedBaseInstance(
            GL_LINE_LOOP,
            shape_offsets[i].size,
            GL_UNSIGNED_SHORT,
            (void*)(sizeof(GLushort) * shape_offsets[i].offset),
            shapes_counts[i],
            j
        );
        j += shapes_counts[i];
    }
}

void AsteroidField::upload_instance_data() {
    for (size_t i = 0; i < SHAPES_COUNT; ++i)
        shapes_counts[i] = 0;
    for (auto& a : this->objects)
        ++shapes_counts[a->shape_id];
    size_t p = 0;
    size_t indices[SHAPES_COUNT];
    for (size_t i = 0; i < SHAPES_COUNT; ++i) {
        indices[i] = p;
        p += shapes_counts[i];
    }
    vbo_instances_data.resize(p);
    for (auto& a : this->objects)
        vbo_instances_data[indices[a->shape_id]++] = a->get_vbo_entry();

    glInvalidateBufferData(this->vbo_instances);
    glBindBuffer(GL_ARRAY_BUFFER, this->vbo_instances);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vbo_entry_t)*vbo_instances_data.size(),
                 &vbo_instances_data[0], GL_STREAM_DRAW);
}

void Ship::update(float dt) {
    auto inp = Asteroids::g_game->get_input(this->input_source);
    this->velocity += acceleration * dt * inp.accelerate *
        glm::vec2(cosf(theta), sinf(theta));
    Asteroids::g_game->get_particle_renderer()->spawn_burst(
        this->position - 0.015f * glm::vec2(cosf(theta), sinf(theta)),
        glm::vec3(1.0, 0.4, 0.0), glm::vec3(0, 0.4, 0),
        this->theta-pi, 0.3, 0.8 * Random::get_nf(0.2, 0.05),
        this->velocity, (inp.accelerate > 0) * ((int)(inp.accelerate * 10) + 1)
    );
    // int mod = 10, threshold = 4;
    // bool t = (int)(glfwGetTime() * 100) % mod < threshold;
    this->angular_velocity += angular_acceleration * dt * (inp.left - inp.right);

    if (inp.left > 0.5f) {
        Asteroids::g_game->get_particle_renderer()->spawn_burst(
            this->position + 0.02f * glm::vec2(cosf(theta-pi/7), sinf(theta-pi/7)),
            glm::vec3(0.8), glm::vec3(0.2),
            this->theta-pi/2, 0.2, 0.5 * Random::get_nf(0.2, 0.05),
            this->velocity, 1
        );
    }
    if (inp.right > 0.5f) {
        Asteroids::g_game->get_particle_renderer()->spawn_burst(
            this->position + 0.02f * glm::vec2(cosf(theta+pi/7), sinf(theta+pi/7)),
            glm::vec3(0.8), glm::vec3(0.2),
            this->theta+pi/2, 0.2, 0.5 * Random::get_nf(0.2, 0.05),
            this->velocity, 1
        );
    }
    this->position += this->velocity * dt;
    this->theta += this->angular_velocity * dt;
    this->angular_velocity *= this->angular_drag;
}

void Asteroid::update(float dt) {
    position += velocity * dt;
    if (abs(position.x) > 1.1 ||
        abs(position.y) > 1/Asteroids::g_game->get_ar()+0.1) {
        this->dead = true;
        return;
    }
    theta += angular_velocity * dt;
}

void AsteroidField::update(float dt) {
    static float acc = 0.0f;
    const int target_count = 10;
    for (size_t i = 0; i < objects.size(); ++i) {
        Polygon *a = objects[i];
        a->update(dt);
        for (auto &b : objects) {
            if (a->type() == OBJECT_SHIP && b->type() == OBJECT_SHIP) continue;
            if (a == b) continue;
            if (glm::length(a->position - b->position) < 0.7*(a->size+b->size)) {
                a->color = b->color = glm::vec3(1.0, 0.0, 1.0);
            }
        }
        if (a->is_dead()) {
            delete a;
            if (i != objects.size()-1) {
                objects[i] = objects[objects.size()-1];
            }
            i--;
            objects.pop_back();
        }
    }

    while (objects.size() < target_count && acc <= 0) {
        objects.push_back(Asteroid::make_random());
        acc += Random::get_nf(0.01f, 0.005f);
    }
    acc -= dt;
    upload_instance_data();
}

void AsteroidField::prepare_shapes() {
    // ship shape
    ebo_shapes_data.push_back(12);
    ebo_shapes_data.push_back(17);
    ebo_shapes_data.push_back(24);
    ebo_shapes_data.push_back(19);
    shape_offsets.push_back((ofs_t){0, 4});

    size_t o = 4;
    for (size_t i = 1; i < SHAPES_COUNT; ++i) {
        size_t c = 0;
        int m[12] = {0};
        for (size_t j = 0; j < MIN_ASTEROID_VERTICES; ++j) {
            // set MIN_ASTEROID_VERTICES random bits in m[]. This is not
            // the best solution.
            int k = -1;
            while (true) {
                k = Random::get_i(0, 11);
                if (m[k]) continue;
                else {
                    m[k] = true;
                    break;
                }
            }
        }
        for (size_t j = 0; j < 12; ++j) {
            int r = Random::get_i(0, 2);
            if (r < 2 || m[j]) {
                ebo_shapes_data.push_back(j + 12 * Random::get_i(0, 1));
                c++;
            }
        }
        shape_offsets.push_back((ofs_t){o, c});
        o += c;
    }
}

const size_t color_count = 4;
glm::vec3 player_colors[color_count] {
    glm::vec3(0.1, 0.4, 1.0),
    glm::vec3(1.0, 0.2, 0.1),
    glm::vec3(0.1, 1.0, 0.2),
    glm::vec3(1.0, 1.0, 0.1),
};

void AsteroidField::init() {
    prepare_shapes();
    create_ships();

    this->shader_program = new ShaderProgram(RESOURCE_DIR "asteroids.v.glsl",
                                             RESOURCE_DIR "asteroids.f.glsl");

    shader_attr_locations.geometry =
        glGetAttribLocation(shader_program->get_id(), "geometry");
    shader_attr_locations.vertex =
        glGetAttribLocation(shader_program->get_id(), "vertex");
    shader_attr_locations.color =
        glGetAttribLocation(shader_program->get_id(), "color");
    shader_uniform_locations.ar =
        glGetUniformLocation(shader_program->get_id(), "ar");

    glGenVertexArrays(1, &this->vao);

    {
        GLuint buffers[3];
        glGenBuffers(3, buffers);
        this->vbo_vertices = buffers[0];
        this->vbo_instances = buffers[1];
        this->ebo_shapes = buffers[2];
    }

    glBindBuffer(GL_ARRAY_BUFFER, this->vbo_vertices);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLData::vbo_asteroid_vertices_data),
                 GLData::vbo_asteroid_vertices_data, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->ebo_shapes);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                 sizeof(GLushort) * ebo_shapes_data.size(),
                 &ebo_shapes_data[0], GL_STATIC_DRAW);

    glBindVertexArray(this->vao);

    glEnableVertexAttribArray(shader_attr_locations.geometry);
    glEnableVertexAttribArray(shader_attr_locations.color);
    glEnableVertexAttribArray(shader_attr_locations.vertex);

    glBindBuffer(GL_ARRAY_BUFFER, this->vbo_vertices);
    glVertexAttribPointer(shader_attr_locations.vertex, 2, GL_FLOAT,
                          GL_FALSE, 2 * sizeof(GLfloat), NULL);
    glBindBuffer(GL_ARRAY_BUFFER, this->vbo_instances);
    glVertexAttribPointer(
        shader_attr_locations.geometry, 4, GL_FLOAT, GL_FALSE,
        sizeof(vbo_entry_t), NULL
    );
    glVertexAttribPointer(
        shader_attr_locations.color, 4, GL_FLOAT, GL_FALSE,
        sizeof(vbo_entry_t), (void*)(4 * sizeof(GLfloat))
    );
    glVertexAttribDivisor(shader_attr_locations.geometry, 1);
    glVertexAttribDivisor(shader_attr_locations.color, 1);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->ebo_shapes);
    glBindVertexArray(0);

    glUseProgram(this->shader_program->get_id());
    glUniform1f(this->shader_uniform_locations.ar, Asteroids::g_game->get_ar());
}

void AsteroidField::reset_ships() {
    for (size_t i = 0; i < NPLAYERS; ++i) {
        ships[i]->position = glm::vec2(0.f, 0.f);
        ships[i]->velocity = glm::vec2(0.f, 0.f);
        ships[i]->angular_velocity = 0.f;
        ships[i]->theta = 0.f;
        ships[i]->color = (i < color_count) ?
            player_colors[i] : glm::vec3(1.0, 1.0, 1.0);
    }
}

void AsteroidField::create_ships() {
    for (size_t i = 0; i < NPLAYERS; ++i) {
        Ship* ship = new Ship();
        ships[i] = ship;
        ship->shape_id = 0; // TODO make this a constant variable
        ship->size = 0.7 * 0.05;
        ship->input_source = (input_mode_t)i;
        objects.push_back(ship);
    }
    reset_ships();
}

void AsteroidField::destroy() {
    glDeleteVertexArrays(1, &this->vao);
    GLuint buffers[] = {
        this->vbo_vertices,
        this->vbo_instances,
        this->ebo_shapes
    };
    glDeleteBuffers(3, buffers);
    delete this->shader_program;
}

AsteroidField::~AsteroidField() {
    for (auto &a : objects) {
        delete a;
    }
}

Asteroid* Asteroid::make_random_at(int shape_id, glm::vec2 position) {
    Asteroid* a = new Asteroid;
    a->position = position;
    a->shape_id = shape_id;
    auto V = (glm::vec2(Random::get_f(-0.7, 0.7),
                        Random::get_f(-0.7, 0.7)) - a->position);
    a->velocity = normalize(V) * Random::get_f(0.3, 0.8);
    a->theta = Random::get_f(-pi, pi);
    a->angular_velocity = Random::get_f(-pi, pi);
    a->size = 0.05 * Random::get_f(0.5, 1.0);
    return a;
}

Asteroid* Asteroid::make_random() {
    const float rw = 1.1;
    const float rh = 0.6;
    glm::vec2 position;
    int shape_id = Random::get_i(1, SHAPES_COUNT-1);
    int s = Random::get_i(0, 3);
    float x = Random::get_f(-rw, rw);
    float y = Random::get_f(-rh, rh);
    switch (s) {
    case 0:
        position = glm::vec2(-rw, y);
        break;
    case 1:
        position = glm::vec2(rw, y);
        break;
    case 2:
        position = glm::vec2(x, -rh);
        break;
    case 3:
        position = glm::vec2(x, rh);
        break;
    }
    return Asteroid::make_random_at(shape_id, position);
}