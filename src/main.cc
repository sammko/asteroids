#include <stdexcept>
#include <memory>

#include "game.h"

#if defined(_WIN32)
#include <windows.h>
#else
#include <iostream>
#endif

int main() {
    try {
        std::unique_ptr<Asteroids::Game> game(new Asteroids::Game());
        game->run();
    } catch (std::runtime_error &e) {
#if defined(_WIN32)
        MessageBox(NULL, e.what(), NULL, MB_OK);
#else
        std::cerr << e.what() << std::endl;
#endif
        return 1;
    }
}