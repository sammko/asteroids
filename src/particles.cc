
#include "particles.h"
#include "resources.h"
#include "util.h"
#include "game.h"

using namespace Asteroids;

void Particle::update(float dt) {
    this->position += dt * this->velocity;
    this->life -= dt;
    if (this->life < 0) {
        this->dead = true;
    }
}

void ParticleRenderer::update(float dt) {

    for (size_t i = 0; i < particles.size(); ++i) {
        particles[i].update(dt);
        if (particles[i].dead) {
            particles[i] = particles[particles.size() - 1];
            particles.pop_back();
            --i;
        }
    }

    if (Random::get_i(0, 100) < 50) {
        // random space noise
        Particle p;
        p.position = glm::vec2(Random::get_f(-1, 1), Random::get_f(-0.5, 0.5));
        p.velocity = glm::vec2(Random::get_f(-1, 1), Random::get_f(-0.5, 0.5));
        p.color = glm::vec3(1.0, 1.0, 1.0);
        p.life = 1;
        p.dead = false;
        particles.push_back(p);
    }

    vbo_particles_data.resize(0);


    for (auto &a : particles) {
        vbo_particles_data.push_back(a.get_vbo_entry());
    }

    glInvalidateBufferData(this->vbo);
    glBindBuffer(GL_ARRAY_BUFFER, this->vbo);
    glBufferData(GL_ARRAY_BUFFER,
                 sizeof(particle_vbo_entry_t) * vbo_particles_data.size(),
                 &vbo_particles_data[0], GL_STATIC_DRAW);
}

void ParticleRenderer::draw() {
    glUseProgram(this->shader->get_id());
    glBindVertexArray(this->vao);
    glDrawArrays(GL_POINTS, 0, vbo_particles_data.size());
}

void ParticleRenderer::init() {
    this->shader = new ShaderProgram(RESOURCE_DIR "particles.v.glsl",
                                     RESOURCE_DIR "particles.f.glsl");

    this->shader_attr_locations.color =
        glGetAttribLocation(this->shader->get_id(), "color");
    this->shader_attr_locations.position =
        glGetAttribLocation(this->shader->get_id(), "position");
    this->shader_uniform_locations.ar =
        glGetUniformLocation(this->shader->get_id(), "ar");

    glUseProgram(this->shader->get_id());
    glUniform1f(this->shader_uniform_locations.ar, Asteroids::g_game->get_ar());

    glGenVertexArrays(1, &this->vao);
    glGenBuffers(1, &this->vbo);

    glBindVertexArray(this->vao);
    glBindBuffer(GL_ARRAY_BUFFER, this->vbo);

    glEnableVertexAttribArray(this->shader_attr_locations.color);
    glEnableVertexAttribArray(this->shader_attr_locations.position);

    glVertexAttribPointer(this->shader_attr_locations.color, 4, GL_FLOAT,
                          GL_FALSE, sizeof(particle_vbo_entry_t), NULL);
    glVertexAttribPointer(this->shader_attr_locations.position, 4, GL_FLOAT,
                          GL_FALSE, sizeof(particle_vbo_entry_t),
                          (void*)(sizeof(glm::vec4)));

}

void ParticleRenderer::destroy() {
    delete this->shader;
}

void ParticleRenderer::spawn_burst(glm::vec2 position, glm::vec3 color,
                                   glm::vec3 color_deviation,
                                   float theta, float dispersion, float life,
                                   glm::vec2 base_velocity, size_t count)
{
    for (size_t i = 0; i < count; ++i) {
        Particle p;
        p.dead = false;
        p.position = position;
        float r = Random::get_nf(0, 1);
        p.color = color + r * color_deviation;
        float a = Random::get_nf(theta, dispersion);
        p.velocity = 0.2f * glm::vec2(cosf(a), sinf(a)) + base_velocity;
        p.life = life;
        this->particles.push_back(p);
    }
}