#include <random>
#include "util.h"

namespace Asteroids {
    namespace Random {
        std::default_random_engine random_engine;

        void seed(unsigned seed) {
            random_engine.seed(seed);
        }

        float get_f(float min, float max) {
            std::uniform_real_distribution<float> d(min, max);
            return d(random_engine);
        }

        int get_i(int min, int max) {
            std::uniform_int_distribution<int> d(min, max);
            return d(random_engine);
        }

        float get_nf(float mean, float sdev) {
            std::normal_distribution<float> d(mean, sdev);
            return d(random_engine);
        }
    }
}