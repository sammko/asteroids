cmake_minimum_required(VERSION 3.0)
project(Asteroids)

set(OpenGL_GL_PREFERENCE GLVND)

# setup deps
set(GLFW_ROOT "${CMAKE_SOURCE_DIR}/deps/glfw")
set(GLFW_INC "${GLFW_ROOT}/include")

set(GLAD_ROOT "${CMAKE_SOURCE_DIR}/deps/glad")
set(GLAD_INC "${GLAD_ROOT}/include")
set(GLAD_SRC "${GLAD_ROOT}/src/glad.c")

set(GLM_ROOT "${CMAKE_SOURCE_DIR}/deps/glm")
set(GLM_INC "${GLM_ROOT}")

# asteroids target
file(GLOB CCSOURCES src/*.cc)
add_executable(asteroids ${CCSOURCES} ${GLAD_SRC})


# c++ flags
set(CMAKE_CXX_STANDARD 14)

if(MINGW)
    set(CMAKE_EXE_LINKER_FLAGS "-static -static-libgcc -static-libstdc++")
    set_target_properties(asteroids PROPERTIES LINK_SEARCH_END_STATIC 1)
    target_link_libraries(asteroids -mwindows)
endif()

set(CMAKE_C_FLAGS_RELEASE "${CMAKE_C_FLAGS_RELEASE} -s -O2")
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -s -O2")


# glfw
set(GLFW_BUILD_DOCS OFF CACHE BOOL "" FORCE)
set(GLFW_BUILD_TESTS OFF CACHE BOOL "" FORCE)
set(GLFW_BUILD_EXAMPLES OFF CACHE BOOL "" FORCE)
add_subdirectory(${GLFW_ROOT})

find_package(OpenGL REQUIRED)

# asteroids
include_directories(
    include
    ${GLFW_INC}
    ${GLAD_INC}
    ${GLM_INC}
)
target_link_libraries(asteroids glfw ${OPENGL_gl_LIBRARY})


add_custom_target(copy_resources ALL
    COMMAND ${CMAKE_COMMAND} -E copy_directory
        ${CMAKE_SOURCE_DIR}/resources ${CMAKE_CURRENT_BINARY_DIR}/resources
    COMMAND ${CMAKE_COMMAND} -E copy
        ${CMAKE_SOURCE_DIR}/deps/SDL_GameControllerDB/gamecontrollerdb.txt
        ${CMAKE_CURRENT_BINARY_DIR}/resources
)

add_custom_target(zip DEPENDS copy_resources asteroids
    COMMAND ${CMAKE_COMMAND} -E tar "cfv" "asteroids.zip" --format="zip"
        "${CMAKE_CURRENT_BINARY_DIR}/$<TARGET_FILE_NAME:asteroids>"
        "${CMAKE_CURRENT_BINARY_DIR}/resources"
)
