

# Build requirements
Build tested under GNU/Linux and mingw-w64 (both native and cross-compile).

 - a C compiler
 - a C++ compiler
 - make
 - CMake
 - Python (for the gl3w generator)

Requirements of [glfw](http://www.glfw.org/) apply, as it is also built.

# Procedure
Fetch submodules:
```
git submodule init
git submodule update
```
Generate build files:
```
mkdir build
cd build
cmake ..
```
Build:
```
make
```

You should end up with an `asteroids` binary in the build directory.
